import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { ApiResponse } from '../model/api-response';
import { MovieModel } from '../model/movie-model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(
    private http: HttpClient
  ) { }

  getPopularMovies(pageIndex?: number): Promise<ApiResponse> {
    return new Promise((resolve, reject) => {
      let url = `${environment.API_BASE_PATH}/movie/popular?api_key=${environment.API_KEY}&language=pt-BR`;
      
      if (pageIndex) 
        url += `&page=${pageIndex}`;

      // Executa a chamada da api
      this.http.get<ApiResponse>(url).subscribe(data => {
        resolve(data);
      });
    })
  }

  getMovieDetails(id: number): Promise<MovieModel> {
    return new Promise((resolve, reject) => {
      let url = `${environment.API_BASE_PATH}/movie/${id}?api_key=${environment.API_KEY}&language=pt-BR`;

      // Executa a chamada da api
      this.http.get<MovieModel>(url).subscribe(data => {
        resolve(data);
      });
    })
  }
}
