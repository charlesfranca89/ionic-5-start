import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CardModel } from '../../../core/model/card-model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {
// sinal de = atribui valor
  // sinal : para tipar a variavel
  // title:string = "Madison, WI";
  // subtitle:string = "Destination";
  // content:string = "Founded in <strong>1829</strong> on an isthmus between Lake Monona and Lake Mendota, Madison was named the capital of the Wisconsin Territory in 1836.";
  // image:string = "https://blog.emania.com.br/wp-content/uploads/2017/02/2-2.jpg";
  textColor:string = 'red';

  @Input() card: CardModel;
  @Output() onCardClicked = new EventEmitter();
  @Input() defaultBehavior = true;
  
  constructor(
    private route: Router
  ) { }

  ngOnInit() {
    
  }

  cardClicked() {
    if (this.defaultBehavior) {
      this.route.navigate(['/home'])
    } else {
      this.onCardClicked.emit(this.card);
    }
  }

}
