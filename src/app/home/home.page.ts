import { Component, OnInit } from '@angular/core';
import { CardModel } from '../core/model/card-model';
import { MovieService } from '../core/services/movie.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  movieModel: CardModel = {} as CardModel;
  movies: Array<CardModel> = [];
  loading = true;

  constructor(
    private movieService: MovieService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getMovies();
  }

  getMovies() {
    this.movieService
      .getMovies()
      .then(data => this.movies = data)
      .finally(() => this.loading = false);
  }

  saveMovie() {
    this.movieService.addMovie(this.movieModel);
    this.movieModel = {} as CardModel;
    this.getMovies();
  }

  movieClicked(card: CardModel) {
    this.router.navigateByUrl(`/home-details/${card.id}`);
  }
}
